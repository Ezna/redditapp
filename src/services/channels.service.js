const baseUrl = 'http://localhost:3001';

class ChannelService{

    static async list(){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json"
            }
        }

        let call = await fetch(`${baseUrl}/channels`, init);
        return call;
    }

    static async details(id){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
            }
        }

        let call = await fetch(`${baseUrl}/channels/${id}`, init);
        return call;
    }

    static async update(id, body){
        let data = new FormData();
        for ( let property in body ) {
            body[property] && data.append(property, body[property]);
        }
        let init={
            method: "PUT",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: data
        }
        let call = await fetch(`${baseUrl}/channels/${id}`, init);
        return call;
    }

    static async create(body){
        let data = new FormData();
        for ( let property in body ) {
            body[property] && data.append(property, body[property]);
        }


        let init={
            method: "POST",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: data
        }
        let call = await fetch(`${baseUrl}/channels`, init);
        return call;
    }

    static async delete(id){
        let init={
            method: "DELETE",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }

        let call = await fetch(`${baseUrl}/channels/${id}`, init);
        return call;
    }


    static async listAdmins(id){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }

        let call = await fetch(`${baseUrl}/channels/admins/${id}`, init);
        return call;
    }

    static async addAdmin(id, body){
        let init={
            method: "PUT",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }

        let call = await fetch(`${baseUrl}/channels/admins/${id}`, init);
        return call;
    }

    static async deleteAdmin(id, body){
        let init={
            method: "DELETE",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }

        let call = await fetch(`${baseUrl}/channels/admins/${id}`, init);
        return call;
    }

    static async listSubscribers(id){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }

        let call = await fetch(`${baseUrl}/channels/subscribers/${id}`, init);
        return call;
    }

    static async addSubscriber(id, body){
        let init={
            method: "PUT",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }

        let call = await fetch(`${baseUrl}/channels/subscribers/${id}`, init);
        return call;
    }

    static async deleteSubscriber(id, body){
        let init={
            method: "DELETE",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }

        let call = await fetch(`${baseUrl}/channels/subscribers/${id}`, init);
        return call;
    }

}

export default ChannelService;