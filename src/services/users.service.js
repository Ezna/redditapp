const baseUrl= "http://localhost:3001";


class UserService{

    static async list(){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
            }
        }

        let call = await fetch(`${baseUrl}/users`, init);
        return call;
    }

    static async delete(id){
        let init={
            method: "DELETE",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }

        let call = await fetch(`${baseUrl}/users/${id}`, init);
        return call;
    }

    static async details(id){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
            }
        }

        let call = await fetch(`${baseUrl}/users/${id}`, init);
        return call;
    }

    static async update(id, body){
        let data = new FormData();
        for ( let property in body ) {
            body[property] && data.append(property, body[property]);
        }


        let init={
            method: "PUT",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: data,
        }
        let call = await fetch(`${baseUrl}/users/${id}`, init);
        return call;
    }



    static async create(body){
        let data = new FormData();
        for ( let property in body ) {
            body[property] && data.append(property, body[property]);
        }


        let init={
            method: "POST",
            headers: {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: data,
        }
        let call = await fetch(`${baseUrl}/users`, init);
        return call;
    }

    static async auth(body){
        let init={
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(body)
        }   
        let call = await fetch(`${baseUrl}/users/authentificate`, init);
        return call;
    }

    static async ban(id, body){
        let init={
            method: "PUT",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }
        let call = await fetch(`${baseUrl}/users/ban/${id}`, init);
        return call;
    }

    static async listSubscriptions(id){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }
        let call = await fetch(`${baseUrl}/users/subscriptions/${id}`, init);
        return call;
    }

}

export default UserService;