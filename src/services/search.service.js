const baseUrl= "http://localhost:3001";


class SearchService{

    static async search(keyword){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
            }
        }

        let call = await fetch(`${baseUrl}/search/${keyword}`, init);
        return call;
    }

}

export default SearchService;