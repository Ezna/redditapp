const baseUrl= "http://localhost:3001";


class ReportService{

    static async list(){
        let init={
            method: "GET",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }

        let call = await fetch(`${baseUrl}/reports`, init);
        return call;
    }

    static async delete(id){
        let init={
            method: "DELETE",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        }

        let call = await fetch(`${baseUrl}/reports/${id}`, init);
        return call;
    }


    static async create(body){

        let init={
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify(body)
        }

        let call = await fetch(`${baseUrl}/reports`, init);
        return call;
    }

}

export default ReportService;