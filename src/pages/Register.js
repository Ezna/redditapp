import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserService from '../services/users.service';

class Register extends Component {


    state = {
        avatar: '',
        nickname: '',
        firstname: '',
        lastname: '',
        desription: '',
        email: '',
        password: ''
    }

    handleChange(e) {
        if (e.target.files) {
            this.setState({
                avatar: e.target.files[0]
            });
        } else {
            this.setState({
                [e.target.id]: e.target.value
            });
        }
    }

    componentDidMount() {
        localStorage.getItem('token') && this.props.history.push('/');
    }


    // Ajout d'un utilisateur
    async submit(e) {
        e.preventDefault();
        let response = await UserService.create(this.state);

        if (response.ok) {
            let data = await response.json();
            localStorage.setItem('token', data.token);
            localStorage.setItem('user', JSON.stringify(data.user));
            this.props.authentificated(true);
            this.props.history.push('/');
        } else {
            console.log('non');
        }
    }

    render() {
        let { avatar } = this.state;
        return (
            <div className="sign-in-page" >
                <div className="signin-popup" >
                    <div className="signin-popup" >
                        <div className="row" >
                            <div className="col-lg-6" >
                                <div className="cmp-info">
                                    <div className="cm-logo d-flex flex-column">
                                        <img className="logo-sidebar align-self-center" src="/workwise/images/wd-logo.png" alt="" />
                                        <p>Workwise,  is a global freelancing platform and social networking where businesses and independent professionals connect and collaborate remotely</p>
                                    </div>
                                    <img src="/workwise/images/cm-main-img.png" alt="" />
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="login-sec">
                                    <div className="sign_in_sec current">
                                        <h3>Inscription</h3>
                                        <form onSubmit={(e) => this.submit(e)}>
                                            <div className="row">
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" require id="firstname" type="text" onChange={(e) => this.handleChange(e)} placeholder='Prénom' />
                                                        <i className="la la-user"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" require id="lastname" type="text" onChange={(e) => this.handleChange(e)} placeholder='Nom' />
                                                        <i className="la la-user"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        {
                                                            (typeof avatar !== "string") ?
                                                                (
                                                                    <img className='img-preview mb-2' src={`${URL.createObjectURL(avatar)}`} alt={''} />
                                                                )
                                                                :
                                                                ('')
                                                        }
                                                        <input className="form-control" id="avatar" type="file" onChange={(e) => this.handleChange(e)} />
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" require id="nickname" type="text" onChange={(e) => this.handleChange(e)} placeholder='Pseudo' />
                                                        <i className="la la-user"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <textarea className="form-control" id="description" type="text" onChange={(e) => this.handleChange(e)} placeholder='Description' />
                                                        <i className="la la-pen"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" required id="email" type="text" onChange={(e) => this.handleChange(e)} placeholder='Email' />
                                                        <i className="la la-user"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" required id="password" type="password" onChange={(e) => this.handleChange(e)} placeholder='Mot de passe' />
                                                        <i className="la la-lock"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <button type="submit" className="btn btn-primary">Inscription</button>
                                                </div>

                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuth: state.isAuth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authentificated: is => {
            dispatch({ type: 'AUTHENTIFICATED', is: is })
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);