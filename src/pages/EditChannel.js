import React, { Component } from 'react';
import ChannelService from '../services/channels.service';
import { connect } from 'react-redux';

class EditChannel extends Component {

    state = {
        name: '',
        avatar: '',
        ownerId: '',
    }

    async componentDidMount() {
        let { id } = this.props.match.params;
        let response = await ChannelService.details(id);

        if (response.ok) {
            let data = await response.json();
            this.setState({ name: data.channel.name, ownerId: data.channel.ownerId._id, avatar: data.channel.avatar });
        }
    }

    componentDidUpdate() {
        let isOwner = false;
        if (this.props.isAuth) {
            isOwner = JSON.parse(localStorage.getItem('user'))._id === this.state.ownerId._id && true;
        }

        if (!isOwner && !this.props.isSuperAdmin) {
            this.props.history.push('/');
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.files ? e.target.files[0] : e.target.value
        });
    }

    // Modification d'un channel
    async submit(e) {
        e.preventDefault();
        let { id } = this.props.match.params;
        let response = await ChannelService.update(id, this.state);
        if (response.ok) {
            this.props.history.push('/');
        } else {
            console.log('non');
        }

    }

    render() {
        let { name, avatar } = this.state;
        let { baseUrl } = this.props;
        return (
            <main>
                <div className="main-section">
                    <div className="container">
                        <div className="main-section-data">
                            <div className="row">
                                <div className="col-lg-8 no-pd">
                                    <div className="row">
                                        <div className="post-bar sign_in_sec current">
                                            <form onSubmit={(e) => this.submit(e)}>
                                                <div className="row">
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <input className="form-control" required id="name" type="text" onChange={(e) => this.handleChange(e)} placeholder='Nom' value={name} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <img className='img-preview mb-2' src={(typeof avatar === "string") ? `${baseUrl}/${avatar}` : `${URL.createObjectURL(avatar)}`} alt={''} />
                                                            <input className="form-control" id="avatar" type="file" onChange={(e) => this.handleChange(e)} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <button type="submit">Modifier</button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 no-pd">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}
const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};
export default connect(mapStateToProps)(EditChannel);