import React, { Component } from 'react';
import ChannelService from '../services/channels.service';
import UserService from '../services/users.service';
import { connect } from 'react-redux';
import User from '../components/User';



class AdminsChannel extends Component {


    state = {
        admins: [],
        users: [],
        owner:''
    }

    async componentDidMount() {
        let { id } = this.props.match.params;
        let response1 = await ChannelService.listAdmins(id);
        if (response1.ok) {
            let data = await response1.json();

            this.setState({ admins: data.admins , owner: data.owner}, () => {
                let isOwner = false;
                if (this.props.isAuth) {
                    isOwner = JSON.parse(localStorage.getItem('user'))._id === this.state.owner && true;
                }
        
                if (!isOwner) {
                    this.props.history.push('/');
                }
            });
        }else{
            this.props.history.push('/');
        }

        let response2 = await UserService.list();
        if (response2.ok) {
            let data = await response2.json();
            let users = data.users.filter(user => user._id !== this.state.owner && this.state.admins.findIndex(admin => admin._id === user._id) === -1);
            this.setState({ users: users });
        }
    }


    // Ajout / suppresion d'un admin sur le channel
    async setAdmin(e,isAdmin, userId){
        e.preventDefault();
        let { id } = this.props.match.params;
        let body = {
            adminId : userId
        };
        let admins = this.state.admins;
        let users = this.state.users;
        if(isAdmin){
            let response = await ChannelService.deleteAdmin(id,body);

            if (response.ok) {
                let indexAdmin = admins.findIndex(admin => admin._id === userId);
                users.push(admins[indexAdmin]);
                admins.splice(indexAdmin,1);
                this.setState({ admins: admins , users: users});
            }
        }else{
            let response = await ChannelService.addAdmin(id,body);

            if (response.ok) {
                let indexUser = users.findIndex(user => user._id === userId);
                admins.push(users[indexUser]);
                users.splice(indexUser,1);
                this.setState({ admins: admins , users: users});
            }
        }
    }

    // Ban d'un utilisateur
    async banUser(e, id, banned){
        e.preventDefault();
        let body = {
            banned : banned
        }
        let response = await UserService.ban(id, body);

        if (response.ok) {
            let users = this.state.users;
            let admins = this.state.admins;
            let indexUser = users.findIndex(user => user._id === id);
            let indexAdmin = admins.findIndex(admin => admin._id === id);
            if(indexUser !== -1){
                users[indexUser].banned = banned;
            }
            if(indexAdmin !== -1){
                admins[indexAdmin].banned = banned;
            }
            
            this.setState({ users: users, admins : admins });
        }
    }

    render() {
        let { admins, users } = this.state;

        return (
            <main>
                <section className='companies-info'>
                    <div className="container">
                        <div className="company-title p-0">
                            <h3>Administateurs</h3>
                        </div>
                        <div className="companies-list">
                            <div className="row">
                                {
                                    admins.map(item => {
                                        item.isChannel = true;
                                        item.isAdmin = true;
                                        return (
                                            <User key={item._id} data={item} ban={(e, id, banned) => this.banUser(e, id, banned)} set={(e,isAdmin,userId) => this.setAdmin(e,isAdmin,userId)} />
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="company-title p-0 mt-4">
                            <h3>Utilisateurs</h3>
                        </div>
                        <div className="companies-list">
                            <div className="row">
                                {
                                    users.map(item => {
                                        item.isChannel = true;
                                        item.isAdmin = false;
                                        return (
                                            <User key={item._id} data={item} ban={(e, id, banned) => this.banUser(e, id, banned)} set={(e,isAdmin,userId) => this.setAdmin(e,isAdmin,userId)} />
                                        )
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    
                </section>
            </main>
        )
    }

}

const mapStateToProps = state => {
    return {
        isAuth: state.isAuth,
        baseUrl: state.baseUrl
    };
};

export default connect(mapStateToProps)(AdminsChannel);