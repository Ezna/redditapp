import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PostService from '../services/posts.service';
import Comment from '../components/Comment';
import Date from '../components/Date';
import { connect } from 'react-redux';
import CommentService from '../services/comments.service';
import ChannelService from '../services/channels.service';
import Swal from 'sweetalert2';


class DetailPost extends Component {


    state = {
        title: '',
        content: '',
        date: '',
        vote: '',
        user: '',
        channel: '',
        image: '',
        comments: [],
        admins: [],
        newComment: {
            userId: '',
            postId: '',
            content: ''
        }
    }

    handleChange(e) {
        this.setState({
            newComment: {
                ...this.state.newComment,
                [e.target.id]: e.target.files ? e.target.files[0] : e.target.value
            }
        });
    }

    async componentDidMount() {
        let { id } = this.props.match.params;
        if (this.props.isAuth) {
            this.setState({
                newComment: {
                    ...this.state.newComment,
                    userId: JSON.parse(localStorage.getItem('user'))._id,
                    postId: id
                }
            });
        }

        let response = await PostService.details(id);
        if (response.ok) {
            let data = await response.json();
            this.setState({ user: data.post.userId, channel: data.post.channelId, admins: data.post.channelId.adminsId, title: data.post.title, content: data.post.content, image: data.post.image, date: data.post.date, vote: data.post.vote, comments: data.comments });
        }
    }

    // Ajout d'un commentaire
    async submit(e) {
        e.preventDefault();
        let response = await CommentService.create(this.state.newComment);

        if (response.ok) {
            let data = await response.json();
            let newComments = this.state.comments;
            newComments.push(data.comment);
            this.setState({ comments: newComments });
        } else {
            console.log('non');
        }
    }

    // Ajout d'un vote
    async vote(e) {
        e.preventDefault();
        let { id } = this.props.match.params;
        let user = JSON.parse(localStorage.getItem('user'));
        let voteIndex = user.votes.findIndex(vote => vote === id)
        let vote = voteIndex === -1 ? 1 : -1;

        let body = {
            vote: vote,
            userId: user._id
        }
        let response = await PostService.vote(id, body);

        if (response.ok) {
            let data = await response.json();
            let user = JSON.parse(localStorage.getItem('user'));
            vote === 1 ? user.votes.push(id) : user.votes.splice(voteIndex, 1);
            localStorage.setItem('user', JSON.stringify(user));
            this.setState({ vote: data.post.vote });
        } else {
            console.log('non');
        }
    }

    // Suppresion d'un commentaire
    async deleteComment(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await CommentService.delete(id);

                if (response.ok) {
                    let comments = this.state.comments;
                    comments.splice(comments.findIndex(comment => comment._id === id), 1);
                    this.setState({ comments: comments });
                }
            }
        })


    }

    // Mise une jour d'un commentaire
    async setComment(e, id, content) {
        e.preventDefault();
        let body = {
            content: content
        }
        let response = await CommentService.update(id, body);

        if (response.ok) {
            let comments = this.state.comments;
            let indexComment = comments.findIndex(comment => comment._id === id);

            comments[indexComment].content = content;
            comments[indexComment].isEditing = false;

            this.setState({ comments: comments });
        } else {
            console.log('non');
        }
    }

    // Demander à l'utilisateur de se connecter pour effectuer l'action
    async goToLogin(e) {
        e.preventDefault();
        Swal.fire({
            title: 'Vous devez être connecter pour effectuer cette action',
            text: "Voulez-vous vous connecter ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Se connecter',
            cancelButtonText: 'Retour'
        }).then((result) => {
            if (result.value) {
                this.props.history.push('/login');
            }
        })

    }

    // Suppresion d'un channel
    async deleteChannel(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await ChannelService.delete(id);

                if (response.ok) {
                    this.props.history.push('/');
                }
            }
        })
    }

    render() {
        let { title, content, vote, date, user, comments, channel, image, admins } = this.state;
        let { id } = this.props.match.params;
        let { isAuth, baseUrl, isBanned, isSuperAdmin } = this.props;
        let currentUser = JSON.parse(localStorage.getItem('user'));

        let active = '';
        if (isAuth) {
            active = currentUser.votes.findIndex(vote => vote === id) !== -1 ? 'active' : '';
        }

        let isOwner = false;
        let isAdmin = false;
        if (isAuth) {
            isOwner = currentUser._id === user._id && true;
            isAdmin = (admins.indexOf(currentUser._id) !== -1) && true;
        }
        return (
            <main>
                <div className="main-section">
                    <div className="container">
                        <div className="main-section-data">
                            <div className="row">
                                <div className="col-xl-9 col-lg-9 col-md-12">
                                    <div className="main-ws-sec">
                                        <div className="post-bar">
                                            <div className="post_topbar">
                                                <div className="usy-dt">
                                                    <img src={`${baseUrl}/${user.avatar}`} alt="" />
                                                    <div className="usy-name">
                                                        <h3>{title}</h3>
                                                        <p><img src="/workwise/images/clock.png" alt="" /><Date data={date} /></p>
                                                        <Link to={`/users/${user._id}`}>/{user.nickname}</Link>
                                                    </div>
                                                </div>
                                                {
                                                    isOwner || isSuperAdmin || isAdmin ?
                                                        (
                                                            <div className="ed-opts">
                                                                <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                                                <ul className="ed-options">
                                                                    <li><Link to={`/post/edit/${id}`} title="">Modifier</Link></li>
                                                                    <li><Link to={''} onClick={(e) => this.props.delete(e, id)} title="">Supprimer</Link></li>
                                                                </ul>
                                                            </div>
                                                        )
                                                        :
                                                        ('')
                                                }
                                            </div>
                                            <div className="job_descp accountnone d-flex flex-column">
                                                {
                                                    image ?
                                                        (
                                                            <div className='mt-4'>
                                                                <img src={`${baseUrl}/${image}`} alt="" />
                                                            </div>
                                                        )
                                                        :
                                                        ('')
                                                }


                                                <p className='mt-4'>{content}</p>
                                            </div>
                                            <div className="job-status-bar btm-line">
                                                <ul className="like-com">
                                                    <li>
                                                        <Link to={'/'} className={active} onClick={isAuth ? (e) => this.vote(e) : (e) => this.goToLogin(e)}><i className="fas fa-heart"></i>{vote}</Link>
                                                    </li>
                                                    <li><Link to={''} className="com"><i className="fas fa-comment-alt"></i>{`Commentaires ${comments.length}`}</Link></li>
                                                </ul>
                                            </div>
                                            <p>Ajouter un commentaire</p>
                                            {
                                                comments.map(item => {
                                                    item.admins = channel.adminsId;
                                                    return (
                                                        <Comment key={item._id} data={item} delete={(e, id) => this.deleteComment(e, id)} set={(e, id, content) => this.setComment(e, id, content)} />
                                                    )
                                                })
                                            }
                                            {
                                                isAuth && !isBanned ?
                                                    (
                                                        <div className="postcomment">
                                                            <div className="row">
                                                                <div className="col-md-2">
                                                                    <img src={`${baseUrl}/${JSON.parse(localStorage.getItem('user')).avatar}`} alt="" />
                                                                </div>
                                                                <div className="col-md-8">
                                                                    <div className="form-group">
                                                                        <input type="text" className="form-control" id="content" onChange={(e) => this.handleChange(e)} placeholder="Commentaire" />
                                                                    </div>
                                                                </div>
                                                                <div className="col-md-2">
                                                                    <Link to={''} onClick={e => this.submit(e)}>Envoyé</Link>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                    :
                                                    ('')
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-3 no-pd">
                                    <div className="main-left-sidebar no-margin">
                                        <div className="user-data full-width">
                                            <div className="user-profile">
                                                {
                                                    isOwner ?
                                                        (
                                                            <div className="ed-opts">
                                                                <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                                                <ul className="ed-options">
                                                                    <li><Link to={`/channel/edit/${channel._id}`} title="">Modifier</Link></li>
                                                                    <li><Link to={''} onClick={(e) => this.deleteChannel(e, channel._id)} title="">Supprimer</Link></li>
                                                                    <li><Link to={`/channel/admins/${channel._id}`} title="">Administrateurs</Link></li>
                                                                </ul>
                                                            </div>
                                                        )
                                                        :
                                                        ('')
                                                }

                                                <div className="username-dt">
                                                    <div className="usr-pic">
                                                        <img src={`${baseUrl}/${channel.avatar}`} alt="" />
                                                    </div>
                                                </div>
                                                <div className="user-specs">
                                                    <h3><Link to={`/channels/${channel._id}`}>{channel.name}</Link></h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }

}

const mapStateToProps = state => {
    return {
        isAuth: state.isAuth,
        baseUrl: state.baseUrl,
        isBanned: state.isBanned,
        isSuperAdmin: state.isSuperAdmin
    };
};

export default connect(mapStateToProps)(DetailPost);