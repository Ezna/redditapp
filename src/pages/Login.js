import React, {Component} from 'react';
import UserService from '../services/users.service';
import { connect } from 'react-redux';
import toastr from 'toastr';


class Login extends Component{

    state= {
        email: '',
        password: ''
    }

    handleChange(e){
        this.setState({
            [e.target.id] : e.target.value
        })
    }
    componentDidMount(){
        localStorage.getItem('token') && this.props.history.push('/');
    }

    // Connexion utilisateur
    async submit(e){
        e.preventDefault();
        let response = await UserService.auth(this.state);

        if(response.ok){
            let data = await response.json();
            localStorage.setItem('token',data.token);
            localStorage.setItem('user',JSON.stringify(data.user));
            this.props.authentificated(true);
            this.props.history.push('/');
        }else{
            if(response.status === 401){
                toastr.error("L'utilisateur ou le mot de passe ne sont pas corrects");
            }
            console.log('non');
        }
    }

    render(){
        return(
            <div className="sign-in-page" >
                <div className="signin-popup" >
                    <div className="signin-popup" >
                        <div className="row" >
                            <div className="col-lg-6" >
                                <div className="cmp-info">
                                    <div className="cm-logo d-flex flex-column">
                                        <img className="logo-sidebar align-self-center" src="/workwise/images/wd-logo.png" alt=""/>
                                        <p>Workwise,  is a global freelancing platform and social networking where businesses and independent professionals connect and collaborate remotely</p>
                                    </div>	
                                    <img src="/workwise/images/cm-main-img.png" alt=""/>			
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="login-sec">
                                    <div className="sign_in_sec current">
                                        <h3>Connexion</h3>
                                        <form onSubmit={(e) => this.submit(e)}>
                                            <div className="row">
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" required id="email" type="email" onChange={(e) => this.handleChange(e)} placeholder='Email'/>
                                                        <i className="la la-user"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <div className="sn-field">
                                                        <input className="form-control" required id="password" type="password" onChange={(e) => this.handleChange(e)} placeholder='Mot de passe'/>
                                                        <i className="la la-lock"></i>
                                                    </div>
                                                </div>
                                                <div className="col-lg-12 no-pdd">
                                                    <button type="submit">Connexion</button>
                                                </div>
                                                
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return{
        isAuth: state.isAuth
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authentificated: is => {
            dispatch({type: 'AUTHENTIFICATED', is: is})
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);