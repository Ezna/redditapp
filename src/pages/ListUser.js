import React, { Component } from 'react';
import { connect } from 'react-redux';
import User from '../components/User';
import UserService from '../services/users.service';



class ListUser extends Component {


    state = {
        users: []
    }

    async componentDidMount() {
 
        let response = await UserService.list();
        if (response.ok) {
            let data = await response.json();

            this.setState({users: data.users});
        }
        
    }

    // Ban / déban d'un utilisateur
    async banUser(e, id, banned){
        e.preventDefault();
        let body = {
            banned : banned
        }
        let response = await UserService.ban(id, body);

        if (response.ok) {
            let users = this.state.users;
            let indexUser = users.findIndex(user => user._id === id);
            users[indexUser].banned = banned;
            this.setState({ users: users });
        }
    }

    render() {
        let {users} = this.state;

        return (
            <main className="pt-3">
                <section className='companies-info'>
                    <div className="container">
                        {
                            users.length > 0 ?
                                (
                                    <div className="row">
                                        <div className="company-title p-0">
                                            <h3>Utilisateurs</h3>
                                        </div>
                                        {
                                            users.map(item => {
                                                item.isChannel = false;
                                                return (
                                                    <User key={item._id} data={item} ban={(e, id, banned) => this.banUser(e, id, banned)} />
                                                )
                                            })
                                        }
                                    </div>

                                )
                                :
                                ('')
                        }
                    </div>
                </section>
            </main>
        )
    }

}

const mapStateToProps = state => {
    return {
        isSuperAdmin: state.isSuperAdmin
    };
};

export default connect(mapStateToProps)(ListUser);