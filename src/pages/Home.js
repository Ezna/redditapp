import React, { Component } from 'react';
import ChannelService from '../services/channels.service';
import Channel from '../components/Channel';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import Swal from 'sweetalert2';

class Home extends Component {


    state = {
        title: 'Home',
        keyword: '',
        channels: []
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async componentDidMount() {
        let response = await ChannelService.list();

        if (response.ok) {
            let data = await response.json();
            this.setState({ channels: data.channels });
        }
    }

    // Suppresion d'un channel
    async deleteChannel(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await ChannelService.delete(id);

                if (response.ok) {
                    let channels = this.state.channels;
                    channels.splice(channels.findIndex(channel => channel._id === id), 1);
                    this.setState({ channels: channels });
                }
            }
        })
    }

    // Recherche d'un article, channel, utilisateur avec un mot clé
    search(e, keyword) {
        e.preventDefault();
        this.props.history.push(`/search/${keyword}`);
    }

    // Abonnement / désabonnement d'un utilisateur à un channel
    async subscribe(e, id, userFollow) {
        e.preventDefault();

        let currentUser = JSON.parse(localStorage.getItem('user'));
        let indexChannel = this.state.channels.findIndex(channel => channel._id === id);
        let channels = this.state.channels
        let body = {
            subscriberId: currentUser._id
        };
        if (userFollow) {
            let response = await ChannelService.deleteSubscriber(id, body);

            if (response.ok) {
                let indexSubscriber = this.state.channels[indexChannel].subscribersId.findIndex(subscriber => subscriber === currentUser._id);
                channels[indexChannel].subscribersId.splice(indexSubscriber, 1);            
                this.setState({ channels: channels });
            }
        } else {
            let response = await ChannelService.addSubscriber(id, body);

            if (response.ok) {
                channels[indexChannel].subscribersId.push(currentUser._id);
                this.setState({ channels: channels });
            }
        }
    }

    render() {
        let { keyword } = this.state;
        let { baseUrl, isAuth, isBanned } = this.props;
        return (
            <div>
                <div className="search-sec">
                    <div className="container">
                        <div className="search-box p-0">
                            <form>
                                <input type="text" id="keyword" placeholder="Cherchez un channel, un article, un utilisateur avec un mot clé" onChange={(e) => this.handleChange(e)} value={keyword} />
                                <button onClick={(e) => this.search(e, keyword)} >Chercher</button>
                            </form>
                        </div>
                    </div>
                </div>
                <main className="pt-2">
                    <section className="forum-page">
                        <div className="container">
                            <div className="forum-questions-sec">
                                <div className="row">
                                    <div className="col-lg-8">
                                        {
                                            isAuth && !isBanned ?
                                                (
                                                    <div className="post-topbar">
                                                        <div className="user-picy">
                                                            <img src={`${baseUrl}/${JSON.parse(localStorage.getItem('user')).avatar}`} alt="" />
                                                        </div>
                                                        <div className="post-st">
                                                            <ul>
                                                                <li><Link to={`/channel/new/`} className="" >Créer un channel</Link></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                )
                                                :
                                                ('')
                                        }
                                        <div className="forum-questions">
                                            {
                                                this.state.channels.map(item => {
                                                    return (
                                                        <Channel key={item._id} data={item} subscribe={(e, id, userFollow) => this.subscribe(e, id, userFollow)} delete={(e, id) => this.deleteChannel(e, id)} />
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>
                                    <div className="col-lg-4">
                                        <div className="right-sidebar">
                                            <div className="widget widget-about">
                                                <img className="logo-sidebar mb-2" src="/workwise/images/wd-logo.png" alt="" />
                                                <h3>redditPiaf</h3>
                                                <span>Facere publicae rei etiamsi debeo</span>
                                                {
                                                    !isAuth ?
                                                        (
                                                            <div className="sign_link">
                                                                <h3><Link to={'/register'} title="">S'inscrire</Link></h3>
                                                            </div>
                                                        )
                                                        :
                                                        ('')
                                                }

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isAuth: state.isAuth,
        isBanned: state.isBanned
    };
};

export default connect(mapStateToProps)(Home);
