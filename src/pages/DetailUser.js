import React, { Component } from 'react';
import UserService from '../services/users.service';
import ReportService from '../services/reports.service';
import Post from '../components/Post';
import Comment from '../components/Comment';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import PostService from '../services/posts.service';
import CommentService from '../services/comments.service';
import toastr from 'toastr';
import Swal from 'sweetalert2';


class DetailUser extends Component {


    state = {
        posts: [],
        comments: [],
        isUser: false,
        isEditing: false,
        _id: '',
        avatar: '',
        nickname: '',
        firstname: '',
        lastname: '',
        desription: '',
        email: '',
        password: '',
        banned: ''
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.files ? e.target.files[0] : e.target.value
        });

    }


    // Activer / désactiver mode modification d'un utilisateur
    editUser(e) {
        e.preventDefault();
        this.setState({ isEditing: !this.state.isEditing });
    }

    // Suppresion d'un article
    async deletePost(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await PostService.delete(id);

                if (response.ok) {
                    let posts = this.state.posts;
                    posts.splice(posts.findIndex(post => post._id === id), 1);
                    this.setState({ posts: posts });
                }
            }
        })


    }

    async componentDidMount() {
        let { id } = this.props.match.params;
        let response = await UserService.details(id);
        if (response.ok) {
            let data = await response.json();

            if (localStorage.getItem('user')) {
                let currentUser = JSON.parse(localStorage.getItem('user'));
                if (data.user._id === currentUser._id) {
                    this.setState({ isUser: true });
                }
            }
            this.setState({
                _id: data.user._id,
                avatar: data.user.avatar,
                nickname: data.user.nickname,
                firstname: data.user.firstname,
                lastname: data.user.lastname,
                description: data.user.description,
                email: data.user.email,
                password: data.user.password,
                banned: data.user.banned,
                posts: data.posts,
                comments: data.comments
            });
        }
    }

    // Modification d'un utilisateur
    async submit(e) {
        e.preventDefault();

        let { _id, avatar, nickname, firstname, lastname, email, password, description } = this.state;

        let body = {
            _id,
            avatar,
            nickname,
            firstname,
            lastname,
            email,
            password,
            description
        }

        let response = await UserService.update(_id, body);
        if (response.ok) {
            let data = await response.json();
            localStorage.setItem('user', JSON.stringify(data.user));
            this.setState({ isEditing: false, avatar: data.user.avatar });
        }

    }

    // Suppresion d'un commentaire
    async deleteComment(e, id) {
        e.preventDefault();

        let response = await CommentService.delete(id);

        if (response.ok) {
            let comments = this.state.comments;
            comments.splice(comments.findIndex(comment => comment._id === id), 1);
            this.setState({ comments: comments });
        }
    }

    // Modification d'un commentaire
    async setComment(e, id, content) {
        e.preventDefault();
        let body = {
            content: content
        }
        let response = await CommentService.update(id, body);

        if (response.ok) {
            let comments = this.state.comments;
            let indexComment = comments.findIndex(comment => comment._id === id);

            comments[indexComment].content = content;
            comments[indexComment].isEditing = false;

            this.setState({ comments: comments });
        } else {
            console.log('non');
        }
    }

    // Ban / déban de l'utilisateur
    async banUser(e, id, banned) {
        e.preventDefault();
        let body = {
            banned: banned
        }
        let response = await UserService.ban(id, body);

        if (response.ok) {
            this.setState({ banned: banned });
        }
    }

    // Signaler l'utilisateur
    async reportUser(e) {
        e.preventDefault();
        let body = {
            reporterId: JSON.parse(localStorage.getItem('user'))._id,
            userId: this.state._id
        };
        let response = await ReportService.create(body);



        if (response.ok) {
            toastr.success('Signalement effectué');
        }
    }


    render() {
        let { _id, avatar, nickname, firstname, lastname, email, password, description, comments, posts, isUser, isEditing, banned } = this.state;
        let { baseUrl, isSuperAdmin, isAuth } = this.props;

        return (
            <main>
                <div className="main-section">
                    <div className="container">
                        <div className="main-section-data">
                            <div className="row">
                                <div className="col-lg-8 no-pd">
                                    <div className="row">
                                        {
                                            posts.map(item => {
                                                item.admins = item.channelId.adminsId;
                                                return (
                                                    <Post key={item._id} data={item} delete={(e, id) => this.deletePost(e, id)} />
                                                )
                                            })
                                        }
                                    </div>
                                    <div className="row">
                                        <div className="post-bar">
                                            {
                                                comments.map(item => {


                                                    if (item.postId) {
                                                        item.admins = item.postId.channelId.adminsId;
                                                        return (
                                                            <Comment key={item._id} data={item} delete={(e, id) => this.deleteComment(e, id)} set={(e, id, content) => this.setComment(e, id, content)} />
                                                        )
                                                    } else {
                                                        return false;
                                                    }

                                                })
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 no-pd">
                                    <div className="main-left-sidebar no-margin">
                                        <div className="user-data full-width">
                                            <div className="user-profile">
                                                {
                                                    isAuth && !isUser ?
                                                        (
                                                            <div className="ed-opts">
                                                                <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                                                <ul className="ed-options">
                                                                    {
                                                                        isSuperAdmin ?
                                                                            (

                                                                                banned ?
                                                                                    (
                                                                                        <li><Link to={''} onClick={(e) => this.banUser(e, _id, false)} title="">Déban</Link></li>
                                                                                    )
                                                                                    :
                                                                                    (
                                                                                        <li><Link to={''} onClick={(e) => this.banUser(e, _id, true)} title="">Ban</Link></li>
                                                                                    )

                                                                            )
                                                                            :
                                                                            ('')
                                                                    }

                                                                    <li><Link to={''} onClick={(e) => this.reportUser(e, _id)} title="">Signaler</Link></li>

                                                                </ul>
                                                            </div>
                                                        )
                                                        :
                                                        ('')
                                                }
                                                <div className="username-dt">
                                                    <div className="usr-pic">
                                                        <img src={(typeof avatar === "string") ? `${baseUrl}/${avatar}` : `${URL.createObjectURL(avatar)}`} alt="" />
                                                    </div>
                                                </div>
                                                <div className="user-specs">
                                                    <h3>{nickname}</h3>
                                                    <span>{description}</span>
                                                </div>
                                            </div>
                                            <ul className="user-fw-status">
                                                <li>
                                                    <h4>Articles</h4>
                                                    <span>{posts.length}</span>
                                                </li>
                                                <li>
                                                    <h4>Commentaires</h4>
                                                    <span>{comments.length}</span>
                                                </li>
                                                {
                                                    isEditing ?
                                                        (
                                                            <form onSubmit={(e) => this.submit(e)}>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <input type="text" className="form-control" id="nickname" onChange={(e) => this.handleChange(e)} value={nickname} placeholder='Pseudo' />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <input type="file" className="form-control" id="avatar" onChange={(e) => this.handleChange(e)} />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <input type="text" className="form-control" id="firstname" onChange={(e) => this.handleChange(e)} value={firstname} placeholder='Prénom' />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <input type="text" className="form-control" id="lastname" onChange={(e) => this.handleChange(e)} value={lastname} placeholder='Nom' />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <textarea type="text" className="form-control" id="description" onChange={(e) => this.handleChange(e)} value={description} placeholder='Description' />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <input type="text" className="form-control" id="email" onChange={(e) => this.handleChange(e)} value={email} placeholder='Mail' />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div className="pl-3 pr-3">
                                                                        <input type="password" className="form-control" id="password" onChange={(e) => this.handleChange(e)} value={password} placeholder='Mot de passe' />
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <button className="user-edit mr-2" >Envoyé</button>
                                                                    <Link to={'/'} onClick={(e) => this.editUser(e)} title="">Annuler</Link>
                                                                </li>
                                                            </form>

                                                        ) :
                                                        ('')
                                                }
                                                {
                                                    isUser && !isEditing ?
                                                        (
                                                            <li>
                                                                <Link to={'/'} onClick={(e) => this.editUser(e)} title="">Modifier</Link>
                                                            </li>
                                                        ) :
                                                        ('')
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }

}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};

export default connect(mapStateToProps)(DetailUser);