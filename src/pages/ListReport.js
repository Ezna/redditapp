import React, { Component } from 'react';
import ReportService from '../services/reports.service';
import Report from '../components/Report';
import { connect } from "react-redux";
import Swal from 'sweetalert2';

class ListReport extends Component {


    state = {
        reports: []
    }

    async componentDidMount() {
        if (!this.props.isSuperAdmin) {
            await this.props.history.push('/');
        }
        let response = await ReportService.list();

        if (response.ok) {
            let data = await response.json();
            this.setState({ reports: data.reports });
        }
    }

    // Confirmer la prise en compte du signalement, le supprime
    async deleteReport(e, id) {
        e.preventDefault();

        Swal.fire({
            title: "Signalement pris en compte ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Oui',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await ReportService.delete(id);

                if (response.ok) {
                    let reports = this.state.reports;
                    reports.splice(reports.findIndex(report => report._id === id), 1);
                    this.setState({ reports: reports });
                }
            }
        })


    }

    render() {
        let { reports } = this.state;
        return (
            <main className="pt-2">
                <section className="forum-page">
                    <div className="container">
                        <div className="forum-questions-sec">
                            <div className="row">
                                <div className="col-lg-8">
                                    <div className="forum-questions">
                                        {
                                            reports.length > 0 ?
                                                (
                                                    reports.map(item => {
                                                        return (
                                                            <Report key={item._id} data={item} delete={(e, id) => this.deleteReport(e, id)} />
                                                        )
                                                    })
                                                )
                                                :
                                                (
                                                    <div className="post-bar mb-0">
                                                        <div className="post_topbar">
                                                            <div className="usy-dt">
                                                                <div className="usy-name">
                                                                    <h3>Aucun signalement déclaré</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                )

                                        }
                                    </div>
                                </div>
                                <div className="col-lg-4"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isAuth: state.isAuth,
        isSuperAdmin: state.isSuperAdmin
    };
};

export default connect(mapStateToProps)(ListReport);
