import React, { Component } from 'react';
import { connect } from 'react-redux';
import Channel from '../components/Channel';
import ChannelService from '../services/channels.service';
import UserService from '../services/users.service';
import Swal from 'sweetalert2';



class ListUserSubscription extends Component {


    state = {
        channels: [],
    }

    async componentDidMount() {
        let { id } = this.props.match.params;

        let response = await UserService.listSubscriptions(id);
        if (response.ok) {
            let data = await response.json();

            this.setState({ channels: data.channels }, () => {
                let isOwner = false;
                if (this.props.isAuth) {
                    isOwner = JSON.parse(localStorage.getItem('user'))._id === this.props.match.params.id && true;
                }
                if (!isOwner && !this.props.isSuperAdmin) {
                    this.props.history.push('/');
                }
            });
        } else {
            this.props.history.push('/');
        }
    }


    // Suppresion d'un channel
    async deleteChannel(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await ChannelService.delete(id);

                if (response.ok) {
                    let channels = this.state.channels;
                    channels.splice(channels.findIndex(channel => channel._id === id), 1);
                    this.setState({ channels: channels });
                }
            }
        })


    }

    // Abonnement / désabonnement d'un utilisateur à un channel
    async subscribe(e, id, userFollow) {
        e.preventDefault();

        let currentUser = this.props.match.params.id;
        let indexChannel = this.state.channels.findIndex(channel => channel._id === id);
        let channels = this.state.channels
        let body = {
            subscriberId: currentUser
        };

        console.log(userFollow);

        if (userFollow) {
            let response = await ChannelService.deleteSubscriber(id, body);

            if (response.ok) {
                let indexSubscriber = this.state.channels[indexChannel].subscribersId.findIndex(subscriber => subscriber === currentUser);
                channels[indexChannel].subscribersId.splice(indexSubscriber, 1);
                this.setState({ channels: channels });
            }
        } else {
            let response = await ChannelService.addSubscriber(id, body);

            if (response.ok) {
                channels[indexChannel].subscribersId.push(currentUser);
                this.setState({ channels: channels });
            }
        }
    }



    render() {
        let { channels } = this.state;

        return (

            <main className="pt-3">
                <section className='companies-info'>
                    <div className="container">
                        {
                            channels.length > 0 ?
                                (
                                    <div className="row mb-4">
                                        <div className="company-title p-0">
                                            <h3>Channels</h3>
                                        </div>
                                        <div className="forum-questions">
                                            {
                                                channels.map(item => {
                                                    return (
                                                        <Channel key={item._id} data={item} subscribe={(e, id, userFollow) => this.subscribe(e, id, userFollow)} delete={(e, id) => this.deleteChannel(e, id)} />
                                                    )
                                                })
                                            }
                                        </div>
                                    </div>


                                )
                                :
                                (
                                    <div className="post-bar mb-0">
                                        <div className="post_topbar">
                                            <div className="usy-dt">
                                                <div className="usy-name">
                                                    <h3>Aucun abonnements</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                        }
                    </div>
                </section>
            </main>
        )
    }

}

const mapStateToProps = state => {
    return {
        isAuth: state.isAuth,
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin
    };
};

export default connect(mapStateToProps)(ListUserSubscription);