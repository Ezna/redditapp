import React, { Component } from 'react';
import ChannelService from '../services/channels.service';
import Post from '../components/Post';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PostService from '../services/posts.service';
import Swal from 'sweetalert2';


class DetailChannel extends Component {


    state = {
        name: '',
        slug: '',
        avatar: '',
        owner: '',
        posts: [],
        newPosts: [],
        admins: [],
        keyword: '',
        sortingActive: 'New'
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async componentDidMount() {
        let { id } = this.props.match.params;
        let response = await ChannelService.details(id);
        if (response.ok) {
            let data = await response.json();
            data.posts.map((post, index) => {
                post.comments = data.comments[index];
            });

            this.setState({ owner: data.channel.ownerId, name: data.channel.name, avatar: data.channel.avatar, posts: data.posts, slug: data.channel.slug, admins: data.channel.adminsId });
        }
    }

    // Suppression d'un article
    async deletePost(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await PostService.delete(id);

                if (response.ok) {
                    let posts = this.state.posts;
                    posts.splice(posts.findIndex(post => post._id === id), 1);
                    this.setState({ posts: posts });
                }
            }
        })


    }

    // Recherche avec un mot clé
    search(e, keyword) {
        e.preventDefault();
        this.props.history.push(`/search/${keyword}`);
    }

    // Triage des articles par dates, like, like+commentaire
    sortPost(e, type) {
        e.preventDefault();
        let posts = this.state.posts;
        switch (type) {
            case 'New':
                posts.sort(function (a, b) {
                    let aDate = new Date(a.date);
                    let bDate = new Date(b.date);
                    return bDate - aDate;
                });
                this.setState({ sortingActive: 'New', posts: posts });
                break;
            case 'Hot':
                posts.sort(function (a, b) {
                    return (b.vote + b.comments) - (a.vote + a.comments);
                });
                this.setState({ sortingActive: 'Hot', posts: posts });
                break;
            case 'Top':
                posts.sort(function (a, b) {
                    return b.vote - a.vote;
                });
                this.setState({ sortingActive: 'Top', posts: posts });
                break;
            default:
                break;
        }

    }

    // Suppresion d'un channel
    async deleteChannel(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await ChannelService.delete(id);

                if (response.ok) {
                    this.props.history.push('/');
                }
            }
        })
    }

    

    render() {
        let { posts, slug, name, avatar, owner, admins, keyword, sortingActive } = this.state;
        let { id } = this.props.match.params;
        let { isAuth, baseUrl, isBanned } = this.props;
        let isOwner = false;
        if (isAuth) {
            isOwner = owner._id === JSON.parse(localStorage.getItem('user'))._id && true;
        }

        return (
            <div>
                <div className="search-sec">
                    <div className="container">
                        <div className="search-box p-0">
                            <form>
                                <input type="text" id="keyword" placeholder="Cherchez un channel, un article, un utilisateur avec un mot clé" onChange={(e) => this.handleChange(e)} value={keyword} />
                                <button onClick={(e) => this.search(e, keyword)} >Chercher</button>
                            </form>
                        </div>
                    </div>
                </div>
                <main>
                    <div className="main-section">
                        <div className="container">
                            <div className="main-section-data">
                                <div className="row">
                                    <div className="col-lg-8 no-pd">
                                        <div className="main-ws-sec">
                                            <div className="post-bar">
                                                <div className="job_descp">
                                                    <ul className="skill-tags mb-0">
                                                        <li className='mb-0 mr-2'><Link className={(sortingActive === 'New') ? 'active' : ''} to={''} onClick={(e) => this.sortPost(e, 'New')} title="">New</Link></li>
                                                        <li className='mb-0 mr-2'><Link className={(sortingActive === 'Hot') ? 'active' : ''} to={''} onClick={(e) => this.sortPost(e, 'Hot')} title="">Hot</Link></li>
                                                        <li className='mb-0 mr-2'><Link className={(sortingActive === 'Top') ? 'active' : ''} to={''} onClick={(e) => this.sortPost(e, 'Top')} title="">Top</Link></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            {
                                                isAuth && !isBanned ?
                                                    (
                                                        <div className="post-topbar">
                                                            <div className="user-picy">
                                                                <img src={`${baseUrl}/${JSON.parse(localStorage.getItem('user')).avatar}`} alt="" />
                                                            </div>
                                                            <div className="post-st">
                                                                <ul>
                                                                    <li><Link to={`/${slug}/posts/new/`} className="" >Créer un article</Link></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    )
                                                    :
                                                    ('')
                                            }

                                            <div className="post-section">
                                                {
                                                    posts.map(item => {
                                                        item.admins = admins;
                                                        return (
                                                            <Post key={item._id} data={item} delete={(e, id) => this.deletePost(e, id)} />
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 no-pd">
                                        <div className="main-left-sidebar no-margin">
                                            <div className="user-data full-width">
                                                <div className="user-profile">
                                                    {
                                                        isOwner ?
                                                            (
                                                                <div className="ed-opts">
                                                                    <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                                                    <ul className="ed-options">
                                                                        <li><Link to={`/channel/edit/${id}`} title="">Modifier</Link></li>
                                                                        <li><Link to={''} onClick={(e) => this.deleteChannel(e, id)} title="">Supprimer</Link></li>
                                                                        <li><Link to={`/channel/admins/${id}`} title="">Administrateurs</Link></li>
                                                                        <li><Link to={`/channel/subscribers/${id}`} title="">Abonnées</Link></li>
                                                                    </ul>
                                                                </div>
                                                            )
                                                            :
                                                            ('')
                                                    }

                                                    <div className="username-dt">
                                                        <div className="usr-pic">
                                                            <img src={`${baseUrl}/${avatar}`} alt="" />
                                                        </div>
                                                    </div>
                                                    <div className="user-specs">
                                                        <h3>{name}</h3>
                                                    </div>
                                                </div>
                                                <ul className="user-fw-status">
                                                    <li>
                                                        <h4>Articles</h4>
                                                        <span>{posts.length}</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        isAuth: state.isAuth,
        baseUrl: state.baseUrl,
        isBanned: state.isBanned
    };
};

export default connect(mapStateToProps)(DetailChannel);