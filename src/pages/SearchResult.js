import React, { Component } from 'react';
import SearchService from '../services/search.service';
import { connect } from 'react-redux';
import User from '../components/User';
import PostService from '../services/posts.service';
import Post from '../components/Post';
import Channel from '../components/Channel';
import ChannelService from '../services/channels.service';
import Swal from 'sweetalert2';



class SearchResult extends Component {


    state = {
        channels: [],
        users: [],
        posts: [],
        keyword: ''
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    async componentDidMount() {
        let { keyword } = this.props.match.params;
        let response = await SearchService.search(keyword);
        if (response.ok) {
            let data = await response.json();

            this.setState({ channels: data.channels, users: data.users, posts: data.posts });
        }
    }

    // Suppresion d'un article
    async deletePost(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await PostService.delete(id);

                if (response.ok) {
                    let posts = this.state.posts;
                    posts.splice(posts.findIndex(post => post._id === id), 1);
                    this.setState({ posts: posts });
                }
            }
        })

    }
    // Suppresion d'un channel
    async deleteChannel(e, id) {
        e.preventDefault();

        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await ChannelService.delete(id);

                if (response.ok) {
                    let channels = this.state.channels;
                    channels.splice(channels.findIndex(channel => channel._id === id), 1);
                    this.setState({ channels: channels });
                }
            }
        })

    }
    // Recherche d'un article, channel, utilisateur avec un mot clé
    async search(e, keyword) {
        e.preventDefault();
        let response = await SearchService.search(keyword);
        if (response.ok) {
            let data = await response.json();

            this.setState({ channels: data.channels, users: data.users, posts: data.posts });
        }
    }

    // Abonnement / désabonnement d'un utilisateur à un channel
    async subscribe(e, id, userFollow) {
        e.preventDefault();

        let currentUser = JSON.parse(localStorage.getItem('user'));
        let indexChannel = this.state.channels.findIndex(channel => channel._id === id);
        let channels = this.state.channels
        let body = {
            subscriberId: currentUser._id
        };
        console.log(userFollow);
        if (userFollow) {
            let response = await ChannelService.deleteSubscriber(id, body);

            if (response.ok) {
                let indexSubscriber = this.state.channels[indexChannel].subscribersId.findIndex(subscriber => subscriber === currentUser._id);
                channels[indexChannel].subscribersId.splice(indexSubscriber, 1);
                this.setState({ channels: channels });
            }
        } else {
            let response = await ChannelService.addSubscriber(id, body);

            if (response.ok) {
                channels[indexChannel].subscribersId.push(currentUser._id);
                this.setState({ channels: channels });
            }
        }
    }



    render() {
        let { channels, users, posts, keyword } = this.state;

        return (
            <div>
                <div className="search-sec">
                    <div className="container">
                        <div className="search-box p-0">
                            <form>
                                <input type="text" id="keyword" placeholder="Cherchez un channel, un article, un utilisateur avec un mot clé" onChange={(e) => this.handleChange(e)} value={keyword} />
                                <button onClick={(e) => this.search(e, keyword)} >Chercher</button>
                            </form>
                        </div>
                    </div>
                </div>
                <main className="pt-3">
                    <section className='companies-info'>
                        <div className="container">
                            {
                                channels.length > 0 ?
                                    (
                                        <div className="row mb-4">
                                            <div className="company-title p-0">
                                                <h3>Channels</h3>
                                            </div>
                                            <div className="forum-questions">
                                                {
                                                    this.state.channels.map(item => {
                                                        return (
                                                            <Channel key={item._id} data={item} subscribe={(e, id, userFollow) => this.subscribe(e, id, userFollow)} delete={(e, id) => this.deleteChannel(e, id)} />
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>


                                    )
                                    :
                                    ('')
                            }
                            {
                                posts.length > 0 ?
                                    (
                                        <div className="row mb-4">
                                            <div className="company-title p-0">
                                                <h3>Articles</h3>
                                            </div>
                                            <div className="post-section">
                                                {
                                                    posts.map(item => {
                                                        item.admins = item.channelId.adminsId;
                                                        return (
                                                            <Post key={item._id} data={item} delete={(e, id) => this.deletePost(e, id)} />
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>

                                    )
                                    :
                                    ('')
                            }
                            {
                                users.length > 0 ?
                                    (
                                        <div className="row">
                                            <div className="company-title p-0">
                                                <h3>Utilisateurs</h3>
                                            </div>
                                            {
                                                users.map(item => {
                                                    item.isChannel = false;
                                                    return (
                                                        <User key={item._id} data={item} />
                                                    )
                                                })
                                            }
                                        </div>

                                    )
                                    :
                                    ('')
                            }
                        </div>
                    </section>
                </main>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
        isAuth: state.isAuth,
        baseUrl: state.baseUrl
    };
};

export default connect(mapStateToProps)(SearchResult);