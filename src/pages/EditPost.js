import React, { Component } from 'react';
import PostService from '../services/posts.service';
import { connect } from 'react-redux';

class EditPost extends Component {

    state = {
        title: '',
        content: '',
        image: '',
        userId: '',
        channelId: '',
        admins: []
    }

    async componentDidMount() {
        let { id } = this.props.match.params;
        let response = await PostService.details(id);

        if (response.ok) {
            let data = await response.json();
            this.setState({ title: data.post.title, content: data.post.content, channelId: data.post.channelId._id, userId: data.post.userId._id, image: data.post.image, admins: data.post.channelId.adminsId });
        }
    }

    componentDidUpdate() {
        let isOwner = false;
        let user = JSON.parse(localStorage.getItem('user'));
        if (this.props.isAuth) {
            isOwner = user._id === this.state.userId._id && true;
        }

        if (!isOwner && !this.props.isSuperAdmin) {
            this.props.history.push('/');
        }
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.files ? e.target.files[0] : e.target.value
        });
    }

    // Modification d'un article
    async submit(e) {
        e.preventDefault();
        
        let { id } = this.props.match.params;
        let response = await PostService.update(id, this.state);
        if (response.ok) {
            this.props.history.push(`/channels/${this.state.channelId}`);
        } else {
            console.log('non');
        }

    }

    render() {
        let { title, content, image } = this.state;
        let { baseUrl } = this.props;

        return (
            <main>
                <div className="main-section">
                    <div className="container">
                        <div className="main-section-data">
                            <div className="row">
                                <div className="col-lg-8 no-pd">
                                    <div className="row">
                                        <div className="post-bar sign_in_sec current">
                                            <form onSubmit={(e) => this.submit(e)}>
                                                <div className="row">
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <input className="form-control" required id="title" type="text" onChange={(e) => this.handleChange(e)} placeholder='Titre' value={title} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <textarea className="form-control" required id="content" type="text" onChange={(e) => this.handleChange(e)} placeholder='Contenu' value={content} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            {
                                                                image ?
                                                                    (
                                                                        <img className='img-preview mb-2' src={(typeof image === "string") ? `${baseUrl}/${image}` : `${URL.createObjectURL(image)}`} alt={''} />
                                                                    )
                                                                    :
                                                                    ('')
                                                            }

                                                            <input className="form-control" id="image" type="file" onChange={(e) => this.handleChange(e)} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <button type="submit">Modifier</button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 no-pd">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}
const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};
export default connect(mapStateToProps)(EditPost);