import React, { Component } from 'react';
import { connect } from 'react-redux';
import User from '../components/User';
import ChannelService from '../services/channels.service';
import UserService from '../services/users.service';



class ListChannelSubscribers extends Component {


    state = {
        users: [],
        owner: ''
    }

    async componentDidMount() {

        let response = await ChannelService.listSubscribers(this.props.match.params.id);
        if (response.ok) {
            let data = await response.json();

            this.setState({ users: data.subscribers, owner: data.owner }, () => {
                let isOwner = false;
                if (this.props.isAuth) {
                    isOwner = JSON.parse(localStorage.getItem('user'))._id === this.state.owner && true;
                }
                if (!isOwner) {
                    this.props.history.push('/');
                }
            });
        } else {
            this.props.history.push('/');
        }

    }

    // Ban / déban d'un utilisateur
    async banUser(e, id, banned) {
        e.preventDefault();
        let body = {
            banned: banned
        }
        let response = await UserService.ban(id, body);

        if (response.ok) {
            let users = this.state.users;
            let indexUser = users.findIndex(user => user._id === id);
            users[indexUser].banned = banned;
            this.setState({ users: users });
        }
    }

    render() {
        let { users } = this.state;

        return (
            <main className="pt-3">
                <section className='companies-info'>
                    <div className="container">
                        {
                            users.length > 0 ?
                                (
                                    <div className="row">
                                        <div className="company-title p-0">
                                            <h3>Abonnées</h3>
                                        </div>
                                        {
                                            users.map(item => {
                                                item.isChannel = false;
                                                return (
                                                    <User key={item._id} data={item} ban={(e, id, banned) => this.banUser(e, id, banned)} />
                                                )
                                            })
                                        }
                                    </div>

                                )
                                :
                                (
                                    <div className="post-bar mb-0">
                                        <div className="post_topbar">
                                            <div className="usy-dt">
                                                <div className="usy-name">
                                                    <h3>Il semblerait que personne ne soit abonné à ce channel :S</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                        }
                    </div>
                </section>
            </main>
        )
    }

}

const mapStateToProps = state => {
    return {
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};

export default connect(mapStateToProps)(ListChannelSubscribers);