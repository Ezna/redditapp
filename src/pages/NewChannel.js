import React, {Component} from 'react';
import ChannelService from '../services/channels.service';


class NewChannel extends Component{

    state= {
        name: '',
        avatar: '',
        ownerId: '',
    }

    async componentDidMount(){
        this.setState({ownerId: JSON.parse(localStorage.getItem('user'))._id });
    }

    handleChange(e){
        this.setState({
            [e.target.id]: e.target.files ? e.target.files[0] : e.target.value
        });
    }

    // Ajout d'un channel
    async submit(e){
        e.preventDefault();
        let response = await ChannelService.create(this.state);

        if(response.ok){
            this.props.history.push('/');
        }else{
            console.log('non');
        }
    }

    render(){
        let {avatar} = this.state;
        return(
            <main>
                <div className="main-section">
                    <div className="container">
                        <div className="main-section-data">
                            <div className="row">
                                <div className="col-lg-8 no-pd">
                                    <div className="row">
                                        <div className="post-bar sign_in_sec current">
                                            <form onSubmit={(e) => this.submit(e)}>
                                                <div className="row">
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <input className="form-control" required id="name" type="text" onChange={(e) => this.handleChange(e)} placeholder='Nom'/>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            {
                                                                (typeof avatar !== "string") ?
                                                                (
                                                                    <img className='img-preview mb-2' src={`${URL.createObjectURL(avatar)}`} alt={''} />
                                                                )
                                                                :
                                                                ('')
                                                            }
                                                            <input className="form-control" id="avatar" type="file" onChange={(e) => this.handleChange(e)}/>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <button type="submit">Créer</button>
                                                    </div>
                                                    
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 no-pd">
                                    
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </main>
        )
    }
}


export default NewChannel;