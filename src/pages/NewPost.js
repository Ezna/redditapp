import React, { Component } from 'react';
import PostService from '../services/posts.service';


class NewPost extends Component {

    state = {
        title: '',
        content: '',
        slug: '',
        userId: '',
        image: '',
    }

    async componentDidMount() {
        let { slug } = this.props.match.params;
        this.setState({ slug: slug, userId: JSON.parse(localStorage.getItem('user'))._id });
    }

    handleChange(e) {
        this.setState({
            [e.target.id]: e.target.files ? e.target.files[0] : e.target.value
        });
    }

    // Ajout d'un article
    async submit(e) {
        e.preventDefault();
        let response = await PostService.create(this.state);

        if (response.ok) {
            let data = await response.json();
            this.props.history.push(`/channels/${data.post.channelId}`);
        } else {
            console.log('non');
        }
    }

    render() {
        let { image } = this.state;
        return (
            <main>
                <div className="main-section">
                    <div className="container">
                        <div className="main-section-data">
                            <div className="row">
                                <div className="col-lg-8 no-pd">
                                    <div className="row">
                                        <div className="post-bar sign_in_sec current">
                                            <form onSubmit={(e) => this.submit(e)}>
                                                <div className="row">
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <input className="form-control" required id="title" type="text" onChange={(e) => this.handleChange(e)} placeholder='Titre' />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            <textarea className="form-control" required id="content" type="text" onChange={(e) => this.handleChange(e)} placeholder='Contenu' />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <div className="sn-field">
                                                            {
                                                                (typeof image !== "string") ?
                                                                    (
                                                                        <img className='img-preview mb-2' src={`${URL.createObjectURL(image)}`} alt={''} />
                                                                    )
                                                                    :
                                                                    ('')
                                                            }
                                                            <input className="form-control" id="image" type="file" onChange={(e) => this.handleChange(e)} />
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-12 no-pdd">
                                                        <button type="submit">Créer</button>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-4 no-pd">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        )
    }
}

export default NewPost;