import React, {Component} from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Home from './pages/Home';
import Header from './components/Header';
import Login from './pages/Login';
import Register from './pages/Register';
import 'bootstrap/dist/css/bootstrap.css';
import 'toastr/build/toastr.css';
import './App.scss';
import DetailChannel from './pages/DetailChannel';
import DetailPost from './pages/DetailPost';
import DetailUser from './pages/DetailUser';
import NewPost from './pages/NewPost';
import EditPost from './pages/EditPost';
import NewChannel from './pages/NewChannel';
import EditChannel from './pages/EditChannel';
import AdminsChannel from './pages/AdminsChannel';
import SearchResult from './pages/SearchResult';
import ListReport from './pages/ListReport';
import ListChannelSubscribers from './pages/ListChannelSubscribers';
import ListUser from './pages/ListUser';
import ListUserSubscription from './pages/ListUserSubscription';


class App extends Component{
  
 
  render(){
    return(
      <BrowserRouter>
        <Header />
        <Route exact path="/" component={Home} />
        <Route exact path="/channels/:id" component={DetailChannel} />
        <Route exact path="/channel/new" component={NewChannel} />
        <Route exact path="/channel/edit/:id" component={EditChannel} />
        <Route exact path="/channel/admins/:id" component={AdminsChannel} />
        <Route exact path="/channel/subscribers/:id" component={ListChannelSubscribers} />
        <Route exact path="/posts/:id" component={DetailPost} />
        <Route exact path="/post/edit/:id" component={EditPost} />
        <Route exact path="/:slug/posts/new" component={NewPost} />
        <Route exact path="/users/:id" component={DetailUser} />
        <Route exact path="/users/subscriptions/:id" component={ListUserSubscription} />
        <Route exact path="/users" component={ListUser} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/search/:keyword" component={SearchResult} />
        <Route exact path="/reports" component={ListReport} />
      </BrowserRouter>
      )
  }
}

export default App;
