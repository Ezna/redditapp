import React, { Component } from 'react';
import Date from '../components/Date';
import CommentService from '../services/comments.service';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from "react-router-dom";
import Swal from 'sweetalert2';

class Comment extends Component {

    state = {
        comments: [],
        isAnswering: false,
        isEditing: false,
        content: '',
        answer: {
            userId: '',
            commentId: '',
            content: ''
        }
    }

    async componentDidMount() {

        this.setState({ content: this.props.data.content });
        if (this.props.isAuth || this.props.data.isAuth) {
            this.setState({
                answer: {
                    ...this.state.answer,
                    userId: JSON.parse(localStorage.getItem('user'))._id,
                    commentId: this.props.data._id
                }
            });
        }


        let response = await CommentService.details(this.props.data._id);

        if (response.ok) {
            let data = await response.json();
            this.setState({ comments: data.comments });
        }
    }

    // Mise à jour du contenu de la réponse
    handleChange(e) {
        this.setState({
            answer: {
                ...this.state.answer,
                [e.target.id]: e.target.value
            }
        });
    }
    // Mise à jour du contenu d'un commentaire
    handleChange2(e) {
        this.setState({
            [e.target.id]: e.target.value
        });
    }

    // Activer / désactiver mode réponse
    answer() {
        this.setState({ isAnswering: !this.state.isAnswering });
    }

    // Activer / désactiver mode édition d'un commentaire
    edit(e) {
        e.preventDefault();
        this.setState({ isEditing: !this.state.isEditing });
    }

    // Envoie de la réponse
    async submit(e) {
        e.preventDefault();
        let response = await CommentService.create(this.state.answer);

        if (response.ok) {
            let data = await response.json();
            let newComments = this.state.comments;
            newComments.push(data.comment);
            this.setState({ comments: newComments, isAnswering: false });
        } else {
            console.log('non');
        }
    }

    // Mise à jour d'un commentaire
    async setComment(e, id, content) {
        e.preventDefault();
        let body = {
            content: content
        }
        let response = await CommentService.update(id, body);

        if (response.ok) {
            let comments = this.state.comments;
            let indexComment = comments.findIndex(comment => comment._id === id);

            comments[indexComment].content = content;
            comments[indexComment].isEditing = false;

            this.setState({ comments: comments });
        } else {
            console.log('non');
        }
    }

    // Ajout / Suppresion d'un like
    async like(e) {
        let id = this.props.data._id;
        let user = JSON.parse(localStorage.getItem('user'));
        let likeIndex = user.likes.findIndex(like => like === this.props.data._id)
        let like = likeIndex === -1 ? 1 : -1;

        let body = {
            like: like,
            userId: user._id
        }
        let response = await CommentService.like(id, body);

        if (response.ok) {
            let data = await response.json();
            let user = JSON.parse(localStorage.getItem('user'));
            like === 1 ? user.likes.push(id) : user.likes.splice(likeIndex, 1);
            localStorage.setItem('user', JSON.stringify(user));
            this.props.data.like = data.comment.like;
            this.forceUpdate();
        } else {
            console.log('non');
        }
    }

    // Demander à l'utilisateur de se connecter pour effectuer l'action
    goToLogin(history) {
        Swal.fire({
            title: 'Vous devez être connecter pour effectuer cette action',
            text: "Voulez-vous vous connecter ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Se connecter',
            cancelButtonText: 'Retour'
        }).then((result) => {
            if (result.value) {
                history.push('/login');
            }
        })
    }

    // Suppresion d'un commentaire
    async deleteComment(e, id) {
        e.preventDefault();
        Swal.fire({
            title: 'Etes vous sur ?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Allé ça dégage',
            cancelButtonText: 'Annuler'
        }).then(async (result) => {
            if (result.value) {
                let response = await CommentService.delete(id);

                if (response.ok) {
                    let comments = this.state.comments;
                    comments.splice(comments.findIndex(comment => comment._id === id), 1);
                    this.setState({ comments: comments });
                }
            }
        })

    }

    render() {
        let { _id, content, like, userId: user, date, admins } = this.props.data;
        let { comments, isAnswering, isEditing, content: stateContent } = this.state;
        let isAuth = this.props.data.isAuth != null ? this.props.data.isAuth : this.props.isAuth;
        let baseUrl = this.props.data.baseUrl != null ? this.props.data.baseUrl : this.props.baseUrl;
        let isSuperAdmin = this.props.data.isSuperAdmin != null ? this.props.data.isSuperAdmin : this.props.isSuperAdmin;
        let isBanned = this.props.data.isBanned != null ? this.props.data.isBanned : this.props.isBanned;
        let history = this.props.history != null ? this.props.history : this.props.data.history;
        let currentUser = JSON.parse(localStorage.getItem('user'));

        let isOwner = false;
        let isAdmin = false;
        if (isAuth) {
            isOwner = currentUser._id === user._id && true;
            isAdmin = (admins.indexOf(currentUser._id) !== -1) && true;
        }

        let active = '';
        if (isAuth) {
            active = currentUser.likes.findIndex(like => like === _id) !== -1 && 'active';
        }
        return (
            <div className='comment-area mt-4'>
                <div className="post_topbar">
                    {
                        isOwner || isSuperAdmin || isAdmin ?
                            (
                                <div className="ed-opts">
                                    <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                    <ul className="ed-options">
                                        <li><Link to={``} onClick={(e) => this.edit(e)} title="">Modifier</Link></li>
                                        <li><Link to={''} onClick={(e) => this.props.delete(e, _id)} title="">Supprimer</Link></li>
                                    </ul>
                                </div>
                            )
                            :
                            ('')
                    }
                    <div className="usy-dt">
                        <img src={`${baseUrl}/${user.avatar}`} alt="" />
                        <div className="usy-name">
                            <h3>{user.nickname}</h3>
                            <span><img src="/workwise/images/clock.png" alt="" /><Date data={date} /></span>
                        </div>
                    </div>

                </div>
                <div className="reply-area">
                    {
                        isEditing ?
                            (
                                <div className="row align-items-center mb-2 pt-2">
                                    <div className="col-md-10">
                                        <input className="form-control" required id="content" type="text" onChange={(e) => this.handleChange2(e)} placeholder='Contenu' value={stateContent} />
                                    </div>
                                    <div className="col-md-2">
                                        <Link to={''} className="update" onClick={e => { this.props.set(e, _id, stateContent); this.edit(e); }}>Modifier</Link>
                                    </div>

                                </div>

                            )
                            :
                            (
                                <p className='pt-5 mb-1'>{content}</p>
                            )
                    }

                    <span className={`mr-3 ${active}`} onClick={isAuth ? (e) => this.like(e) : () => this.goToLogin(history)}><i className="fa fa-heart"></i>{like}</span>
                    <span onClick={() => this.answer()} ><i className="la la-mail-reply"></i>Répondre</span>
                    {
                        isAnswering && isAuth && !isBanned ?
                            (
                                <div className="postcomment">
                                    <div className="row">
                                        <div className="col-md-2">
                                            <img src={`${baseUrl}/${JSON.parse(localStorage.getItem('user')).avatar}`} alt="" />
                                        </div>
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <input type="text" className="form-control" id="content" onChange={(e) => this.handleChange(e)} placeholder="Commentaire" />
                                            </div>
                                        </div>
                                        <div className="col-md-2">
                                            <Link to={''} onClick={e => this.submit(e)}>Envoyé</Link>
                                        </div>
                                    </div>
                                </div>
                            )
                            :
                            ('')
                    }
                    {
                        comments.map(item => {
                            item.baseUrl = baseUrl;
                            item.isAuth = isAuth;
                            item.isSuperAdmin = isSuperAdmin;
                            item.admins = admins;
                            item.history = history;
                            return (
                                <Comment key={item._id} data={item} delete={(e, id) => this.deleteComment(e, id)} set={(e, id, content) => this.setComment(e, id, content)} />
                            )
                        })
                    }
                </div>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth,
        isBanned: state.isBanned
    };
};

export default withRouter(connect(mapStateToProps)(Comment));
