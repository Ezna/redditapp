import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


class Header extends Component{



    componentDidMount(){
        // Réglage des variables stocker dans le store redux
        localStorage.getItem('token') && this.props.authentificated(true);   
        localStorage.getItem('token') && this.props.superAdmin();   
        localStorage.getItem('token') && this.props.banned();   
    }

    // Déconnexion de l'utilisateur
    logout(e){
        e.preventDefault();
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.props.authentificated(false);
        this.props.superAdmin();
    }


    render(){
        let {isAuth, baseUrl, isSuperAdmin} = this.props;
        return (
            <header>
                <div className="container">
                    
                    <div className="header-data">
                        <div className="forum-bar">
                            <h2><Link to={'/'} className='site-name'>redditPiaf</Link></h2>
                        </div>
                        
                            
                        {
                            isAuth ? (
                                <div className="user-account">
                                    <div className="user-info">
                                        <img src={`${baseUrl}/${JSON.parse(localStorage.getItem('user')).avatar}`} alt=""/>
                                        <Link to={'#'}  className='mr-3' title="">{JSON.parse(localStorage.getItem('user')).nickname}</Link>
                                        <i className="la la-sort-down"></i>
                                    </div>
                                    <div className="user-account-settingss" id="users">
                                        <h3>Paramètres</h3>
                                        <ul className="us-links">
                                            <li><Link to={`/users/${JSON.parse(localStorage.getItem('user'))._id}`} title="">Profil</Link></li>
                                            <li><Link to={`/users/subscriptions/${JSON.parse(localStorage.getItem('user'))._id}`} title="">Abonnements</Link></li>
                                            {
                                                isSuperAdmin ?
                                                (
                                                    <li><Link to={`/users`} title="">Utilisateurs</Link></li>
                                                )
                                                :
                                                ('')
                                            }
                                            {
                                                isSuperAdmin ?
                                                (
                                                    <li><Link to={`/reports`} title="">Signalements</Link></li>
                                                )
                                                :
                                                ('')
                                            }
                                        </ul>
                                        <h3 className="tc no-margin"><Link  to={'/'} onClick={(e) => this.logout(e)}>Déconnexion</Link></h3>
                                    </div>
                                </div>
                            )
                            : (
                                <div className="login_register">
                                    <ul>
                                        <li><Link to={'/login'}>Connexion</Link></li>
                                        <li><Link to={'/register'}>Inscription</Link></li>
                                    </ul>
                                </div>
                            )
                        }
                    </div>
                </div>
            </header>
        )
    }
        
}


const mapStateToProps = state => {
    return{
        isAuth: state.isAuth,
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin
    };
};

const mapDispatchToProps = dispatch => {
    return {
        authentificated: is => {
            dispatch({type: 'AUTHENTIFICATED', is: is})
        },
        superAdmin: () => {
            dispatch({type: 'SUPER_ADMIN'})
        },
        banned: () => {
            dispatch({type: 'BANNED'})
        }
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Header);