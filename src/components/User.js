import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class User extends Component {

    render() {
        let { _id, nickname, avatar, description, isAdmin, isChannel, banned } = this.props.data;
        let { baseUrl, isSuperAdmin, isAuth } = this.props;
        let isUser = false;
        if (isAuth) {
            isUser = (JSON.parse(localStorage.getItem('user'))._id === _id) && true;
        }

        return (
            <div className="col-lg-3 col-md-4 col-sm-6">
                <div className="company_profile_info">
                    {
                        isSuperAdmin && !isUser ?
                            (
                                <div className="ed-opts">
                                    <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                    <ul className="ed-options">
                                        {
                                            banned ? 
                                            (
                                                <li><Link to={''} onClick={(e) => this.props.ban(e, _id, false)} title="">Déban</Link></li>
                                            )
                                            :
                                            (
                                                <li><Link to={''} onClick={(e) => this.props.ban(e, _id, true)} title="">Ban</Link></li>
                                            )
                                        }
                                        
                                    </ul>
                                </div>
                            )
                            :
                            ('')
                    }
                    <div className="company-up-info">
                        <img src={`${baseUrl}/${avatar}`} alt="" />
                        <h3>{nickname}</h3>
                        <h4>{description}</h4>
                        {
                            isChannel ?
                                (
                                    <ul>
                                        {
                                            isAdmin ?
                                                (
                                                    <li><Link to={''} title="" onClick={(e) => this.props.set(e, true, _id)} className="message-us">Retirer</Link></li>
                                                )
                                                :
                                                (
                                                    <li><Link to={''} title="" onClick={(e) => this.props.set(e, false, _id)} className="follow">Ajouter</Link></li>
                                                )
                                        }
                                    </ul>
                                )
                                :
                                ('')
                        }

                    </div>
                    <Link className="view-more-pro" to={`/users/${_id}`}>voir profil</Link>
                </div>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};

export default connect(mapStateToProps)(User);
