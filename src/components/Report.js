import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Date from '../components/Date';

class Report extends Component {

    render() {
        let { _id, reporterId: reporter, userId: user, postId: post, date } = this.props.data;
        let { baseUrl} = this.props;

        return (
            <div className="usr-question">
                <div className="usr_img">
                    <img src={`${baseUrl}/${reporter.avatar}`} alt="" />
                </div>
                <div className="usr_quest">
                    <h3><Link to={`/users/${reporter._id}`}>{reporter.nickname}</Link></h3>
                    {
                        user ?
                            (
                                <p>Signalement de l'utilisateur <Link to={`/users/${user._id}`}>{user.nickname}</Link></p>
                            )
                            :
                            (
                                <p>Signalement de l'article <Link to={`/posts/${post._id}`}>{post.title}</Link></p>
                            )
                    }
                    <ul className="quest-tags m-0">
                        <li><Link to={''} onClick={(e) => this.props.delete(e,_id)}><i className="fas fa-check"></i></Link></li>
                    </ul>
                </div>
                <span className="quest-posted-time"><i className="fa fa-clock-o"></i><Date data={date} /></span>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl
    };
};



export default connect(mapStateToProps)(Report);
