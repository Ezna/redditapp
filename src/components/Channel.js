import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Date from '../components/Date';

class Channel extends Component {

    render() {
        let { _id, name, avatar, date, ownerId, subscribersId } = this.props.data;
        let { baseUrl, isSuperAdmin, isAuth } = this.props;
        let currentUser = JSON.parse(localStorage.getItem('user'));
        let userFollow = false;
        if(isAuth){
            userFollow = (subscribersId.indexOf(currentUser._id) !== -1) && true;
        }
        
        let isOwner = false;

        if (isAuth) {
            isOwner = currentUser._id === ownerId._id && true;
        }
        console.log(ownerId);
        return (
            <div className="usr-question">
                {
                    isSuperAdmin || isOwner ?
                        (
                            <div className="ed-opts">
                                <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                <ul className="ed-options">
                                    <li><Link to={`/channel/edit/${_id}`} title="">Modifier</Link></li>
                                    <li><Link to={''} onClick={(e) => this.props.delete(e, _id)} title="">Supprimer</Link></li>
                                </ul>
                            </div>
                        )
                        :
                        ('')
                }

                <div className="usr_img">
                    <img src={`${baseUrl}/${avatar}`} alt="" />
                </div>
                <div className="usr_quest">
                    <h3><Link to={`/channels/${_id}`}>{name}</Link></h3>
                    {
                        isAuth && !isOwner ?
                            (
                                <ul className="quest-tags m-0">
                                    <li><Link to={''} onClick={(e) => this.props.subscribe(e, _id, userFollow)} title="">
                                         {
                                             userFollow ?
                                             (
                                                <i className="fas fa-check"></i>
                                             )
                                             :
                                             ('Suivre')
                                         }
                                        </Link></li>
                                </ul>
                            )
                            :
                            ('')
                    }
                </div>
                <span className="quest-posted-time"><i className="fa fa-clock-o"></i><Date data={date} /></span>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};



export default connect(mapStateToProps)(Channel);
