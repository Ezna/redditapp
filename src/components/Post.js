import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Date from '../components/Date';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import PostService from '../services/posts.service';
import ReportService from '../services/reports.service';
import Swal from 'sweetalert2';
import toastr from 'toastr';


class Post extends Component {

    // Ajout / suppresion d'un vote
    async vote(e) {
        e.preventDefault();
        let id = this.props.data._id;
        let user = JSON.parse(localStorage.getItem('user'));
        let voteIndex = user.votes.findIndex(vote => vote === id)
        let vote = voteIndex === -1 ? 1 : -1;

        let body = {
            vote: vote,
            userId: user._id
        }
        let response = await PostService.vote(id, body);

        if (response.ok) {
            let data = await response.json();
            let user = JSON.parse(localStorage.getItem('user'));
            vote === 1 ? user.votes.push(id) : user.votes.splice(voteIndex, 1);
            localStorage.setItem('user', JSON.stringify(user));
            this.props.data.vote = data.post.vote;
            this.forceUpdate();
        } else {
            console.log('non');
        }
    }

    // Demander à l'utilisateur de se connecter pour effectuer l'action
    goToLogin(e) {
        e.preventDefault();

        Swal.fire({
            title: 'Vous devez être connecter pour effectuer cette action',
            text: "Voulez-vous vous connecter ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#e44d3a',
            confirmButtonText: 'Se connecter',
            cancelButtonText: 'Retour'
        }).then((result) => {
            if (result.value) {
                this.props.history.push('/login');
            }
        })

    }
    
    //Signalement d'un article
    async reportPost(e){
        e.preventDefault();
        let body = {
            reporterId: JSON.parse(localStorage.getItem('user'))._id,
            postId: this.props.data._id
        };
        let response = await ReportService.create(body);

        

        if (response.ok) {
           toastr.success('Signalement effectué');
        } 
    }


    render() {
        let { _id, title, content, vote, userId: user, date, image, comments, admins } = this.props.data;
        let { baseUrl, isAuth, isSuperAdmin } = this.props;
        let currentUser = JSON.parse(localStorage.getItem('user'));


        let active = '';
        if (isAuth) {
            active = currentUser.votes.findIndex(vote => vote === _id) !== -1 ? 'active' : '';
        }

        let isOwner = false;
        let isAdmin = false;
        if (isAuth) {
            isOwner = currentUser._id === user._id && true;
            isAdmin = (admins.indexOf(currentUser._id) !== -1) && true;
        }


        return (
            <div className="post-bar">
                <div className="post_topbar">
                    <div className="usy-dt">
                        <img src={`${baseUrl}/${user.avatar}`} alt="" />
                        <div className="usy-name">
                            <h3><Link to={`/posts/${_id}`}>{title}</Link></h3>
                            <p className='mb-2'><img src="/workwise/images/clock.png" alt="" /><Date data={date} /></p>
                            <Link to={`/users/${user._id}`}>/{user.nickname}</Link>
                        </div>

                    </div>
                    {
                        isAuth ?
                            (
                                <div className="ed-opts">
                                    <Link to={''} title="" className="ed-opts-open"><i className="la la-ellipsis-v"></i></Link>
                                    <ul className="ed-options">
                                        {
                                            isOwner || isSuperAdmin || isAdmin ?
                                                (
                                                    <li><Link to={`/post/edit/${_id}`} title="">Modifier</Link></li>
                                                )
                                                :
                                                ('')
                                        }
                                        {
                                            isOwner || isSuperAdmin || isAdmin ?
                                                (
                                                    <li><Link to={''} onClick={(e) => this.props.delete(e, _id)} title="">Supprimer</Link></li>
                                                )
                                                :
                                                ('')
                                        }
                                        {
                                            !isOwner ?
                                                (
                                                    <li><Link to={''} onClick={(e) => this.reportPost(e, _id)} title="">Signaler</Link></li>
                                                )
                                                :
                                                ('')
                                        }

                                        
                                    </ul>
                                </div>
                            )
                            :
                            ('')
                    }

                </div>
                <div className="job_descp d-flex flex-column">
                    {
                        image ?
                            (
                                <div className='mt-4'>
                                    <img src={`${baseUrl}/${image}`} alt="" />
                                </div>
                            )
                            :
                            ('')
                    }


                    <p className='mt-4'>{content}</p>
                </div>
                <div className="job-status-bar">
                    <ul className="like-com">
                        <li>
                            <Link to={'/'} className={active} onClick={isAuth ? (e) => this.vote(e) : (e) => this.goToLogin(e)}><i className="fas fa-heart"></i>{vote}</Link>
                        </li>
                        {
                            comments ?
                                (
                                    <li><Link to={`/posts/${_id}`} className="com"><i className="fas fa-comment-alt"></i> {comments}</Link></li>

                                )
                                :
                                ('')
                        }
                    </ul>
                </div>
            </div>
        )

    }
}

const mapStateToProps = state => {
    return {
        baseUrl: state.baseUrl,
        isSuperAdmin: state.isSuperAdmin,
        isAuth: state.isAuth
    };
};


export default withRouter(connect(mapStateToProps)(Post));
