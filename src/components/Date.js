import React, {Component} from 'react';

class Comment extends Component{

    // Différence entre maintenant et une date en secondes,minutes,heures,jours,semaines,mois,années
    dateDiff(d1, d2){
        let diff = [];
        let t2 = d2.getSeconds();
        let t1 = d1.getSeconds();
        diff.push(['seconde' , parseInt((t2-t1))]);

        t2 = d2.getMinutes();
        t1 = d1.getMinutes();
        diff.push(['minute' , parseInt((t2-t1))]);

        t2 = d2.getHours();
        t1 = d1.getHours();
        diff.push(['heure' , parseInt((t2-t1))]);

        t2 = d2.getTime();
        t1 = d1.getTime();
        diff.push(['jour' , parseInt((t2-t1)/(24*3600*1000))]);
    
        t2 = d2.getTime();
        t1 = d1.getTime();
        diff.push(['semaine' , parseInt((t2-t1)/(24*3600*1000*7))]);
    
        let d1Y = d1.getFullYear();
        let d2Y = d2.getFullYear();
        let d1M = d1.getMonth();
        let d2M = d2.getMonth();
        diff.push(['mois' , (d2M+12*d2Y)-(d1M+12*d1Y)]);
    
        diff.push(['année' , d2.getFullYear()-d1.getFullYear()]);

        return diff;
    }
    // Renvoie la phrase de différence entre aujourd'hui et une date
    sinceDate(from){
        let date = new Date(from);
        let now = new Date();
        
        if(from !== ''){
            let diff = this.dateDiff(date,now);
            let toShow = (value) => {
                let  item = diff[value];
                if(item[1] === 0){
                    if(value === 0){
                        return false;
                    }else{
                        return toShow(value -1);
                    }
                    
                }else{
                    if(item[1] > 1){
                        item[0] += 's';
                    }
                    return item;
                }
            }
            let itemShow = toShow(diff.length - 1);
            if(itemShow[0] === 'seconde' || itemShow[0] === 'secondes'){
                return  `A l'instant`;
            }else{
                return  `Il y a ${itemShow[1]} ${itemShow[0]}`;
            }
        }
        

        return from;
    }
  
    render(){
        let date = this.props.data;


        return (
            <span>{ this.sinceDate(date) }</span>
        )
      
    }
}

export default Comment;
