// Store redux
const initState = {
    baseUrl: "http://localhost:3001",
    isAuth: false,
    isSuperAdmin: false,
    isBanned: false,
};

const rootReducer = (state = initState, action) => {
    switch (action.type) {
        case "AUTHENTIFICATED":
            return {
                ...state,
                isAuth: action.is
            };
        case "SUPER_ADMIN":
            let is = false;
            if(state.isAuth){
                let user = JSON.parse(localStorage.getItem('user'));

                if(user.user_role === 10){
                    is = true;
                }
            }
            return {
                ...state,
                isSuperAdmin: is
            };
        case "BANNED":
            let banned = false;
            if(state.isAuth){
                let user = JSON.parse(localStorage.getItem('user'));
                banned = user.banned;
            }
            return {
                ...state,
                isBanned: banned
            };
        default:
            return state;
    }
};

export default rootReducer;